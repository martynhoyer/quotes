import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";
import { BORDER, COLOR, SPACING, FONT, BREAKPOINTS } from "../constants";
import { hideVisually } from "../utils";

export default function QuoteCard({
  /*
  Prop names just match the key names in the API response for now. There may be 
  a reason to change these in the future. `imageUrl` perhaps would be more 
  descriptive.
  */
  image = undefined,
  quote = undefined,
  character = undefined,
  // sensible default here. Not sure I like the capitalisation, but I'm probably
  // nitpicking too much!
  characterDirection = "Left",
  // this will allow us to do `styled(QuoteCard)` and apply some "layout" if
  // required
  className,
}) {
  /*
  Seems sensible to not bother rendering anything if there isn't a quote in the 
  response. I could make this a required prop, but I don't think I can assume 
  that the API will always return the correct data, so I'll handle that here.
  */
  if (!quote) return null;
  return (
    <Figure className={className}>
      <FlexContainer characterDirection={characterDirection}>
        {/* if there isn't an image in the response, don't render one */}
        {/* 
        I also toyed with the idea of adding `character` as the alt attribute 
        here, but decided against it, in favour the `figcaption`. If I have it 
        in both, it could be announced twice which could be frustrating. I'd 
        argue that the image is decorative in this case, hence the empty alt. 
        I'll go a bit further and hide the entire element from assistive tech 
        with aria-hidden too, to be sure!
        */}
        {image && (
          <ImageContainer aria-hidden>
            <Image src={image} alt="" />
          </ImageContainer>
        )}
        <Blockquote>{quote}</Blockquote>
      </FlexContainer>
      {character && <Figcaption>{character}</Figcaption>}
    </Figure>
  );
}

QuoteCard.propTypes = {
  image: PropTypes.string,
  quote: PropTypes.string,
  character: PropTypes.string,
  characterDirection: PropTypes.string,
  className: PropTypes.string,
};

const Figure = styled.figure`
  /*
  This is a flex container to allow the child flex container to have flex-grow 
  on it, so it stays vertically centred when in the grid parent and the text 
  heights are quite different.
  */
  display: flex;
  flex-direction: column;

  margin: 0;
  padding: ${SPACING.M};
  border: 1px solid ${COLOR.GREY.LIGHT};
  border-radius: ${BORDER.RADIUS.M};
  box-shadow: ${BORDER.SHADOW.M};
`;

/*
Until `gap` is supported in flexbox, we can use the negative margin trick to 
lay out flex children.
*/
const flexGap = SPACING.S;

/*
Function to determine which direction the flex children flow in order to get 
image to face the quote. Normally re-ordering or reversing content can be 
dangerous because of tab order, but there's nothing focusable here and the 
image is removed from the a11y tree so we should be ok.
I used a switch instead of ternary because I don't like to assume that if it's 
not `Right` it must be `Left`. Perhaps the API could return `Bottom`? This 
approach feels more explicit and easily updated.
*/
function flexDirection(characterDirection) {
  switch (characterDirection) {
    case "Right":
      return css`
        flex-direction: row-reverse;
      `;
    case "Left":
    default:
      return css`
        flex-direction: row;
      `;
  }
}

const FlexContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-grow: 1;

  margin: -${flexGap};

  @media (min-width: ${BREAKPOINTS.S}) {
    ${({ characterDirection }) => flexDirection(characterDirection)}
  }
`;

const ImageContainer = styled.div`
  flex-shrink: 0;
  margin: ${flexGap};
  padding-inline: ${SPACING.L};
`;

const Image = styled.img`
  display: block;
  max-width: 100%;
  height: 150px;
  margin-inline: auto;
`;

const Blockquote = styled.blockquote`
  flex-grow: 1;
  margin: ${flexGap};
  font-size: ${FONT.SIZE.M};
  text-align: center;
  /*
  Bit of a guess on the line-height, by eye. No line heights in the constants 
  file to use, so just kept here for now
  */
  line-height: 1.5;

  @media (min-width: ${BREAKPOINTS.S}) {
    text-align: start;
  }
`;

const Figcaption = styled.figcaption`
  ${hideVisually};
`;
