import React, { useState } from "react";
import styled from "styled-components";

import Button from "./Button";
import Header from "./Header";
import { BREAKPOINTS, CONTENT_WIDTH, SPACING } from "../constants";
import fetchQuotes from "../fetchQuotes";
import QuoteCard from "./QuoteCard";
import LoadingSpinner from "./LoadingSpinner";

const StyledContentWrapper = styled.div`
  margin: 0 auto;
  max-width: ${CONTENT_WIDTH};
  padding: ${SPACING.L};
`;

const App = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [isErrored, setIsErrored] = useState(false);
  const [quotesList, setQuotesList] = useState([]);

  async function handleClick() {
    // clear the current quotesList and show the loading spinner instead
    setQuotesList([]);
    setIsLoading(true);
    try {
      const res = await fetchQuotes();
      if (res) {
        setQuotesList(res.data);
        setIsLoading(false);
      } else {
        // if there's no result from the fetch, stop the loading spinner and
        // show an error message
        setIsLoading(false);
        setIsErrored(true);
      }
    } catch (e) {
      // if any errors are caught, stop the loading spinner and show an error
      setIsLoading(false);
      setIsErrored(true);
    }
  }

  return (
    <>
      <Header />

      <StyledContentWrapper>
        <Button disabled={isLoading} onClick={handleClick}>
          Load quotes
        </Button>
        <Content>
          {isErrored && (
            <p role="alert">There was an error, please try again</p>
          )}
          {isLoading && <Spinner />}
          {quotesList.length > 0 && (
            <Grid>
              {quotesList.map(
                ({ quote, image, character, characterDirection }) => (
                  <QuoteCard
                    // the `quote` is probably the most unique thing in the
                    // object, so I'll use it as the key. If there was an `id`
                    // I'd use that instead
                    key={quote}
                    quote={quote}
                    image={image}
                    character={character}
                    characterDirection={characterDirection}
                  />
                )
              )}
            </Grid>
          )}
        </Content>
      </StyledContentWrapper>
    </>
  );
};

export default App;

const Content = styled.div`
  /*
  Cheeky hack to stop the margins collapsing when the loading spinner renders
  */
  display: flex;
  flex-direction: column;

  margin-block-start: ${SPACING.L};
`;

const Spinner = styled(LoadingSpinner)`
  margin-block-start: ${SPACING.L};
  margin-inline: auto;
`;

const Grid = styled.div`
  display: grid;
  gap: ${SPACING.M};

  @media (min-width: ${BREAKPOINTS.S}) {
    grid-template-columns: 1fr 1fr;
  }
`;
