import React from "react";
import PropTypes from "prop-types";
import styled, { keyframes } from "styled-components";
import { COLOR } from "../constants";
import { hideVisually } from "../utils";

export default function LoadingSpinner({
  // this will allow us to do `styled(LoadingSpinner)` and apply some "layout"
  // if required
  className,
}) {
  return (
    <Spinner className={className}>
      {/* 
      I've never really found a great solution for making good, accessible 
      feedback that something is loading, so for now this is just a 
      visuallyHidden piece of text. I don't think it should be a live region, 
      or have some special aria role etc., so I'm leaving it for now.
      */}
      <HiddenText>Loading...</HiddenText>
    </Spinner>
  );
}

LoadingSpinner.propTypes = {
  className: PropTypes.string,
};

const spin = keyframes`
  to {
    transform: rotate(360deg);
  }
`;

const Spinner = styled.div`
  width: 1.5em;
  height: 1.5em;
  border-color: ${COLOR.ACCENT.PRIMARY};
  border-left-color: ${COLOR.ACCENT.SECONDARY};
  border-width: 4px;
  border-style: solid;
  border-radius: 50%;
  animation: ${spin} 0.35s linear infinite;
`;

const HiddenText = styled.span`
  ${hideVisually}
`;
