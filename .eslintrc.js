module.exports = {
  parser: "babel-eslint",
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:import/errors",
    "plugin:jsx-a11y/recommended",
    "prettier",
  ],

  env: {
    browser: true,
    node: true,
    jest: true,
    es6: true,
  },
  plugins: ["react", "jsx-a11y"],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    "import/no-unresolved": 0,
    "no-console": [
      "error",
      {
        allow: ["warn"],
      },
    ],
  },
  settings: {
    react: {
      version: "detect",
    },
  },
};
