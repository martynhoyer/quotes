# Martyn Hoyer Farewill task commentary

Hi Farewill team! I thought I'd create this file to give you a bit of a running commentary of my thoughts, processes and decisions while working through the task. It will be accompanied by comments directly in the code where applicable, but I'm hoping it will add a bit more context to my approach.

Starting now, it will be a chronologically ordered list of my train of thought. Perhaps I'll make some mistakes and need to go back and change something, but I believe being transparent about that is helpful for both of us!

1. First things first, I'd like to get this running locally instead of within Glitch, so I've downloaded it from Glitch (I hope that doesn't create extra work for you!), cleaned up a few unncessary files and committed that. The main reason for running it locally is environment familiarity, but also to allow me to run some tooling like ESLint and Prettier. I'm not _reliant_ on those, but they're useful to maintain consistency with certain standards etc. My config choice is mostly standard with a couple of plugins. You can take a look at the dotfiles if you'd like. Another commit for those (and any changed files based on those) before properly getting started.
2. I should create the commentary markdown file to document my process. Here it is!
3. Ok, let's get started. `npm start` seems like a good place to start...
4. Classic. Got an error. ESLint version mismatch. I forgot that create-react-app ships with ESLint within `react-scripts`. Oh well, that's me trying to be too smart I guess. I'll remove the V7 dependency and let it us V6 from `react-scripts`. Hopefully the plugins will continue to work! Clean `npm i` and we're off.
5. Now to write some code! I'm going to start with the thing I find most fun I think - creating and styling components. I'm going to start with a "card" component for the quotes. Probably just `QuoteCard.js` for now. More comments for that are in the code. I added `prop-types` as a dependency mostly because ESLint complained, but it's definitely useful. I'll discuss TypeScript later on I think!
6. HTML. There's quite a few options for how best to mark up this component/design. I've gone with a `figure`/`blockquote`/`figcaption` approach, because it seems the most semantically correct to me. `figcaption` for the quotee/character name (as opposed to `cite`, because it's not actually a citation), even though it doesn't show on the design, but I've made it visually hidden because I think it's useful to keep in the document for assistive technology users. Because that's there, I decided to go with an empty alt attribute on the image, so not to duplicate the character name.
7. CSS. Normally with a component like this I would try to steer clear of media queries. I have a bit of a (possibly irrational) resistance to media queries these days (so excited for container queries!) especially when it comes to a component approach. In this scenario/design though, media queries can't really be avoided because of the `text-align` change across the breakpoint, as well as the requirement for the image to "face" the quote. I also don't usually name my styled-components as per their HTML element, but in this case, it seemed sensible and descriptive enough. I've preferred CSS logical properties here, and assumed browser support, but would happily write the fallbacks or try to automate them with PostCSS or similar. I've also added a `BREAKPOINTS` object to the `constants` file so we can keep consistency across the app. It just has the one breakpoint for now - an arbitrary `40em` width - I like to use `em`s for breakpoints so they continue to function correctly if the user adjusts the text size.
8. Now for wiring that new component up to the fetchQuotes API. I'll head over to `App.js`.
9. In short, I need to attach a handler to the button which calls the `fetchQuotes` function and displays the result on the page. At first glance I was expecting to have to put a `fetch` call inside a `useEffect` here, but I think it's actually simpler than that. I'll create a `handleClick` function which triggers the fetch and then some pieces of state to track the loading, the data and any errors. For now the loading state is just represented in a `<p>`. I'll create a loading spinner component in a bit. While testing it, the images loading in created a bit of render jank, so I changed the CSS slightly on the image to have a fixed height instead of a `max-height`. Not much I can do about the image width at this stage.
10. Next, loading spinner component. I've got a favourite, simple method for this which I'll pretty much copy from another project. I'll use the colours from `constants` for it. I've added that to the page along with a bit more layout CSS to get the spacing cleaned up.
11. I think all that's remaining now is to put the quote cards into the responsive grid. Should be fairly straight forward. Just need to decide whether that's a separate component, or just some styled components within `App.js`. Probably the latter initially and if it looks too messy, maybe move it. Again this might be somewhere where I'd normally try to avoid media queries and just let the items reflow based on their content, but in this case, I think snapping all the visual changes to one breakpoint makes more sense. The next question in my head is flexbox or grid? I think either's fine for this, but with grid I can be a bit lazier and use `gap` (not quite ready in flexbox) which makes my life a bit easier!
12. Now that the grid view is done, I think the breakpoint is a little low, so I'll adjust that. `60em` feels much better.
13. Just noticed the error message. Maybe that should be styled? Maybe not. It should probably have `role="alert"` though, so I'll add that. I'll skip the styling though, there's no obvious "error" tokens in the constants file, so I'd be guessing, and I'm probably out of time now anyway!
14. I think I'm done! A quick scan over everything, and test in some other browsers, and add a last few comments/fix typos.

## Notes after completion

- I would prefer `border-box` as the `box-sizing` in the global CSS, but changing it results in some visual changes to the header, so I'll leave it alone!
- I think Rainier Wolfcastle's `characterDirection` is wrong? Perhaps a deliberate mistake?!
- I don't have the correct fonts installed and don't think I should spend any time getting webfonts set up, so it doesn't look exactly like the design!
- I mentioned TypeScript when adding the `prop-types` dependency. I'm fairly new to TypeScript (although fully sold on it after using it on some projects) so thought about adding it here, but I think that would have added quite a bit of setup time for such a small app. Given how few prop-types I used, I think that was a good decision. That's not to say I don't want to use it on other projects though!
- Using Storybook or similar could have been useful when building the `QuoteCard` component, rather than rendering it into `App.js` to see how it looks. I really like using Storybook in this way, but decided the setup cost was too much for this small project.
- I'm quite used to having a styled-components theme in place, which allows you to access certain variables via `props.theme` on all styled components. You could potentially inject the entire `constants` object as the theme and not need to import them separately in each file. There are pros and cons to this of course... without TypeScript the autocompletion might not be as nice, and there's possibly some tree-shaking advantages to only importing what you actually use.
- In hindsight, using grid for the `QuoteCard` layout would have probably resulted in some cleaner CSS and HTML, but I started off with flexbox trying to avoid the media queries and then didn't fully refactor it.

### Thoughts and reasonings behind your decisions

This should hopefully be covered by this document and the comments in the code, but I'm happy to answer any questions!

### What went well?

I'm pretty happy with it. It seems to be working as expected.

### What could have gone better?

I went over the 2 hour suggestion, maybe I could have reigned in some of the chatter and concentrated on just writing the code, but I think it's quite valuable.

### Is there anything particular you'd like to come back and improve if you had time? Why?

An obvious thing missing is tests, but I'm not sure that's a feasible (or sensible) thing to implement into what is a very small "toy" app. Perhaps a quick(ish) Cypress E2E test would have been good to add at the start, in a TDD fashion.

Another thing that I thought while working on it was around image placeholders - there's still a bit of a janky reflow sometimes when the images load in. If the fetch response was able to supply some image data like `width` and `height` alongside the URL, we could clean that up.
